#!/bin/bash -ex

# CONFIG
prefix="XQuartz"
suffix=""
munki_package_name="X11"
display_name="X11/XQuartz"
icon_name=""
category="Utilities"
url=`./finder.sh`

# download it (-L: follow redirects)
curl -L -o app.pkg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36' "${url}"

#Mount downloaded DMG to random location
##mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.dmg | awk '/private\/tmp/ { print $NF } '`
##echo Mounted on $mountpoint
##pwd=`pwd`

#create directory for temporary installer to live
##mkdir -p root/private/tmp/XQuartzInstaller

#copy original PKG to temporary directory
##cp "${mountpoint}/XQuartz.pkg" root/private/tmp/XQuartzInstaller/app.pkg

#expand files to retrieve version info
##/usr/sbin/pkgutil --expand "${mountpoint}/XQuartz.pkg" pkg
##mkdir build-root
###(cd build-root; pax -rz -f ../pkg/x11.pkg/Payload)
##(cd build-root; pax -rz -f ../pkg/*.pkg/Payload)

##version=`defaults read "${pwd}/build-root/Applications/Utilities/XQuartz.app/Contents/Info.plist" CFBundleShortVersionString`


/usr/sbin/pkgutil --expand-full app.pkg pkg
mkdir -p build-root/Applications/Utilities
#(cd build-root/Applications; pax -rz -f ../../pkg/*.pkg/Payload)
drive_pkg=$(ls -d pkg/X*.pkg)
cp -R "$drive_pkg"/Payload/Applications/Utilities/*.app build-root/Applications/Utilities
#cp -R pkg/Payload/Applications/Utilities/*.app build-root/Applications/Utilities


#version=`defaults read "${pwd}/build-root/Applications/Utilities/XQuartz.app/Contents/Info.plist" CFBundleShortVersionString`

#Obtain version info
version=`/usr/libexec/PlistBuddy -c "print :CFBundleShortVersionString"  build-root/Applications/Utilities/*.app/Contents/Info.plist`

## Create pkg's
#/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version --scripts scripts app.pkg

#hdiutil detach "${mountpoint}"

# Build pkginfo
/usr/local/munki/makepkginfo app.pkg -f build-root/Applications/Utilities/XQuartz.app --postinstall_script postinstall.sh  > app.plist
plist=`pwd`/app.plist

# Remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.8.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" version "${version}"
defaults write "${plist}" category "${category}"
defaults write "${plist}" unattended_install -bool TRUE


# Obtain display name from Izzy and add to plist
display_name=`/usr/local/bin/displaynametool "${munki_package_name}"`
defaults write "${plist}" display_name -string "${display_name}"

# Obtain description from Izzy and add to plist
description=`/usr/local/bin/descriptiontool "${munki_package_name}"`
defaults write "${plist}" description -string "${description}"

# Obtain category from Izzy and add to plist
category=`/usr/local/bin/cattool "${munki_package_name}"`
defaults write "${plist}" category "${category}"

# Obtain installs array values for the application
app_CFBundleIdentifier="org.macosforge.xquartz.X11"
app_CFBundleName="XQuartz"
app_path="/Applications/Utilities/XQuartz.app"
app_type="application"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
